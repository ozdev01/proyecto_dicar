<?php

namespace App\Http\Controllers;

 use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Http\Request;
    use App\Http\Requests;
    use Illuminate\Support\Facades\Paginator;
    use Illuminate\Pagination\LengthAwarePaginator;
    use DateTime;

class ContactosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()     
    {

       $contactos = DB::table("contactos")->orderby("ultimo_contacto","DESC")->paginate(30);

         return view('contacto/all', ['contactos'=> $contactos]);
    }


    public function buscar(Request $request){


        $contactos = DB::table("contactos")->where("")->orderby("ultimo_contacto","DESC")->paginate(30);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacto/nuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos, $datos);

            try {
                
               $id_contacto = DB::table("contactos")->insertGetId(['nombre'=>$datos['nombre'], 'apellidos'=>$datos['apellidos'], 'fecha_nacimiento'=>$datos['fecha_nacimiento'], 'direccion'=>$datos['direccion'], 'cod_postal'=>$datos['cod_postal'], 'provincia'=>$datos['provincia'], 'email'=>$datos['email'], 'movil_1'=>$datos['movil_1'], 'movil_2'=>$datos['movil_2'], 'DNI'=>$datos['dni']]);

               return response($id_contacto);

            } catch (\Exception $e) {
                
                return response("Error: ".$e);
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contacto = DB::table("contactos")->where("id","=",$id)->first();
        $vehiculos = DB::table("vehiculos")->where("id_cliente","=",$id)->get();
        $provincias = DB::table("provincias")->orderby("nombre","desc")->get();
       

        return view('contacto/show', ['contacto'=> $contacto,'vehiculos'=>$vehiculos,'provincias'=>$provincias]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            if($request->ajax()){

            $datos = array();
            parse_str($request->datos, $datos);

            try {
                
                DB::table("contactos")->where("ID","=",$id)->update(['nombre'=>$datos['nombre'], 'apellidos'=>$datos['apellidos'], 'fecha_nacimiento'=>$datos['fecha_nacimiento'], 'direccion'=>$datos['direccion'], 'cod_postal'=>$datos['cod_postal'], 'provincia'=>$datos['provincia'], 'email'=>$datos['email'], 'movil_1'=>$datos['movil_1'], 'movil_2'=>$datos['movil_2'], 'DNI'=>$datos['dni'],'ultimo_contacto'=>$datos["ultimo_contacto"]]);

               return response("El Contacto se ha editado correctamente");

            } catch (\Exception $e) {
                
                return response("Error: ".$e);
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function vehiculos(Request $request){
        if($request->ajax()){
            $vehiculos = DB::table("vehiculos")->where("id_cliente","=",$request->id_cliente)->get();

            $output = '<button class="btn-link"><i class="fas fa-plus"></i> Nuevo vehículo</button>
                <table class="table" style = "font-size: 13px;">
                        <thead class="table-header">
                            <th>Matrícula</th>
                            <th>Modelo</th>
                            <th>Fecha matr.</th>
                            <th>Asesor</th>
                            <th>Km</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">';

                        foreach($vehiculos as $vehiculo){
                        $output.= '<tr>
                        <td>'.$vehiculo->matricula.'</td>
                        <td>'.$vehiculo->modelo.'</td> 
                        <td>'.date("d-m-Y", strtotime($vehiculo->fecha_matriculacion)).'</td> 
                        <td>'.$vehiculo->asesor.'</td> 
                        <td>'.$vehiculo->kilometros.'</td> 
                        <td><a href="#" class="btn btn-success"><i class="fas fa-edit"></i></a></td>     
                        </tr>';
                        }
                $output.=" </tbody>
                    </table>";

                    return response($output);
        }
    }

    public function llamadas(Request $request){
        if($request->ajax()){
            $llamadas = DB::table("historial_llamadas")->where("id_cliente","=",$request->id_cliente)->get();
            $output = '<table class="table" style = "font-size: 13px;">
                        <thead class="table-header">
                            <th>Matrícula</th>
                            <th>Fecha llamada</th>
                            <th>Revisión</th>
                            <th>Rechazo</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">';

                        foreach($llamadas as $llamada){
                        $output.= '<tr>
                        <td>'.$llamada->matricula.'</td>
                        <td>'.date("d-m-Y", strtotime($llamada->fecha_llamada)).'</td> 
                        <td>'.$llamada->tipo_revision.'</td> 
                        <td>'.$llamada->motivo_rechazo.'</td> 
                        <td><a href="#" class="btn btn-success"><i class="fas fa-edit"></i></a></td>     
                        </tr>';
                        }
                $output.=" </tbody>
                    </table>";

                    return response($output);
        }
    }
}
