<?php

namespace App\Http\Controllers;

 use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Http\Request;
    use App\Http\Requests;
    use Illuminate\Support\Facades\Paginator;
    use Illuminate\Pagination\LengthAwarePaginator;
    use DateTime;


class VehiculosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehiculos = DB::table(DB::raw("vehiculos a,asesores b,tipos_seguro c,via_entrada d, contactos e"))->select(DB::raw("a.*,b.nombre_asesor as 'nom_asesor',c.nombre_tipo as 'nom_tipo',d.nombre_via as 'nom_via',e.datos_personales as 'nom_contacto'"))->whereRaw("a.asesor=b.ID and a.tipo_seguro = c.ID and a.via = d.ID and a.Id_cliente = e.ID")->orderBy('a.fecha_matriculacion',"DESC")->paginate(20);

        return view('vehiculo/all', ['vehiculos'=> $vehiculos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $contactos = DB::table("contactos")->orderby("datos_personales","ASC")->get();
        $tipos_seguro = DB::table("tipos_seguro")->get();
        $via_entrada = DB::table("via_entrada")->get();
        $asesores = DB::table("asesores")->get();

       return view('vehiculo/nuevo', ['contactos'=> $contactos,'tipos_seguro'=> $tipos_seguro,'via_entrada'=> $via_entrada,'asesores'=> $asesores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if($request->ajax()){

            $datos = array();
            parse_str($request->datos, $datos);

            try {
                
               $id_vehiculo = DB::table("vehiculos")->insertGetId(['matricula'=>$datos["matricula"], 'modelo'=>$datos["modelo"], 'fecha_matriculacion'=>$datos["fecha_matriculacion"], 'asesor'=>$datos["asesor"], 'fecha_entrada_taller'=>$datos["fecha_entrada"], 'kilometros'=>$datos["kilometros"], 'via'=>$datos["via"], 'tipo_seguro'=>$datos["tipo_seguro"], 'Id_cliente'=>$datos["contacto"]]);

               $revisiones = DB::table("tipos_revisiones")->get();

               foreach ($revisiones as $key => $revision) {
                   DB::table("revisiones")->insert(['matricula'=>$datos["matricula"], 'tipo'=>$revision->ID]);
               }


               return response($id_vehiculo);

            } catch (\Exception $e) {
                
                return response("Error: ".$e);
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $contactos = DB::table("contactos")->orderby("datos_personales","ASC")->get();
        $tipos_seguro = DB::table("tipos_seguro")->get();
        $via_entrada = DB::table("via_entrada")->get();
        $asesores = DB::table("asesores")->get();
        $vehiculo = DB::table("vehiculos")->where("ID","=",$id)->first();
       

        return view('vehiculo/show', ['vehiculo'=>$vehiculo,'contactos'=> $contactos,'tipos_seguro'=> $tipos_seguro,'via_entrada'=> $via_entrada,'asesores'=> $asesores]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos, $datos);

            try {
                
               DB::table("vehiculo")->where("ID","=",$id)->update(['matricula'=>$datos["matricula"], 'modelo'=>$datos["modelo"], 'fecha_matriculacion'=>$datos["fecha_matriculacion"], 'asesor'=>$datos["asesor"], 'fecha_entrada_taller'=>$datos["fecha_entrada"], 'kilometros'=>$datos["kilometros"], 'via'=>$datos["via"], 'tipo_seguro'=>$datos["tipo_seguro"], 'Id_cliente'=>$datos["contacto"]]);

               return response("El vehiculo se ha editado correctamente");

            } catch (\Exception $e) {
                
                return response("Error: ".$e);
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function revisiones(Request $request){

          if($request->ajax()){

            try {
            $revision = DB::table("revisiones")->where("matricula","=",$request->matricula)->where("tipo","=",$request->tipo)->first();

            $output="<tr>".
                     "<td><input type='date' class='form-control' id='nueva_llamada' value='".strftime('%Y-%m-%d',strtotime($revision->proxima_llamada))."'></td>".
                     "<td><input type='text' class='form-control' id='nuevo_comentario' value='".$revision->comentarios."'></td>".
                     "<td><input type='date' class='form-control' id='nueva_cita' value='".strftime('%Y-%m-%d',strtotime($revision->fecha_cita))."'>".
                     "<td><a class='btn btn-sm btn-success edit_revision' id='".$revision->tipo."' href='#'><i class='fas fa-edit'></i></a></td><tr>";
               return response($output);

            } catch (\Exception $e) {
                
                return response("Error: ".$e);
            }

        }

    }

    public function guardar_revision(Request $request){

        if($request->ajax()){

            try {

                $vehiculo = DB::table("vehiculos")->where("matricula","=",$request->matricula)->first();

            $revision = DB::table("revisiones")->where("matricula","=",$request->matricula)->where("tipo","=",$request->tipo)->update(['proxima_llamada'=>$request->nueva_llamada, 'comentarios'=>$request->nuevo_comentario, 'fecha_cita'=>$request->nueva_cita]);

            DB::table("historial_llamadas")->insert(['id_cliente'=>$vehiculo->Id_cliente, 'matricula'=>$vehiculo->matricula, 'tipo_revision'=>$request->tipo, 'motivo_rechazo'=>$request->nuevo_comentario, 'proxima_llamada_revision'=>$request->nueva_llamada, 'fecha_cita'=>$request->nueva_cita]);

            if($request->nueva_cita != null){
              DB::table("citas")->insert(['fecha_cita'=>$request->nueva_cita, 'matricula'=>$request->matricula, 'tipo'=>$request->tipo]);  
            }
            
               return response("cambios realizados correctamente");

            } catch (\Exception $e) {
                
                return response("Error: ".$e);
            }

        }

    }
}
