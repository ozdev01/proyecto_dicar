<?php

namespace App\Http\Controllers;


 use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Http\Request;
    use App\Http\Requests;
    use Illuminate\Support\Facades\Paginator;
    use Illuminate\Pagination\LengthAwarePaginator;
    use DateTime;

class CitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('citas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function eventos (){

        $citas = DB::table("citas")->get();

        $events = array();


        foreach ($citas as $key => $cita) {
            $arr = array('title' => $cita->matricula, 'start' => $cita->fecha_cita, 'end' => $cita->fecha_cita, 'allDay' => 1);
            $events[] = $arr;

        }
        echo json_encode($events);
    }

    public function cargar_cita(Request $request){
         if($request->ajax()){

            $cita = DB::table("citas")->where("matricula","=",$request->matricula)->first();
            $tipo = DB::table("tipos_revisiones")->where("ID","=",$cita->tipo)->first();

            $output = "<label>Matricula:</label> ".$cita->matricula;
            $output .= "<br><label>Fecha:</label> ".date('d-m-Y', strtotime($cita->fecha_cita));
            $output .= "<br><label>Tipo:</label> ".$tipo->nombre_tipo;

            return response($output);

         }
    }
}
