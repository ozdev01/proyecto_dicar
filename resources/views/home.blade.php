@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Menú principal</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                     <div class="metr" style="margin-left: 43px;">
                         <a class="metrostyle orgmetro" style="cursor: pointer" href="{{URL::to('/contacto/nuevo')}}">

                             <span class="fas fa-user-plus" style="font-size: 4em; color: white; padding-left: 0.3em; margin-top: 3px;float:left"></span>
                             
                             <span style="color: white; float: left; margin-top: 10px; margin-left: 10px;margin-right:40px;font-size: 13px;">Nuevo contacto</span>
                         </a>
                         <a class="metrostyle eenmetro" style="cursor: pointer" href="{{URL::to('/contacto/allitems')}}">

                             <span class="fas fa-users" style="font-size: 4em; color: white; padding-left: 0.3em; margin-top: 3px ;float:left"></span>
                         
                             <span style="color: white; float: left; margin-top: 10px; margin-left: 10px ;margin-right:10px;font-size: 13px;">Todos los contactos</span>

                         </a>
                         <a style="cursor: pointer" class="metrostyle boometro" href="{{URL::to('/vehiculo/nuevo')}}">
                                <span class="fas fa-plus-square" style="font-size:4em;color:white;padding-left:0.3em;margin-top:3px;float:left"></span>
                                 
                                <span style="color:white;float:left;margin-top:10px;margin-left:10px;margin-right:40px;font-size: 13px;">Nuevo vehículo </span>
                            </a>

                            <a style="cursor: pointer" class="metrostyle yoometro" href="{{URL::to('/vehiculo/allitems')}}">
                                <span class="fas fa-car" style="font-size:4em;color:white;padding-left:0.3em;margin-top:3px;float:left"></span>
                                 
                                <span style="color:white;float:left;margin-top:10px;margin-left:10px;margin-right:0px;font-size: 13px;">Todos los vehículos </span>
                            </a>
                        

                     </div>
                      <div class="metr" style="margin-left: 43px">
                         <a class="metrostyle reemetro" style="cursor: pointer" href="{{URL::to('/historial')}}">
                             <span class="fas fa-phone" style="font-size: 4em; color: white; padding-left: 0.3em ; margin-top: 3px ;float:left"></span>
                             <span style="color: white; float: left; margin-top: 10px; margin-left: 10px;margin-right:80px;font-size: 13px">Histórico </span>
                         </a>

                         <a class="metrostyle yoometro" style="cursor: pointer" href="{{URL::to('/revisiones')}}">

                             <span class="fas fa-wrench" style="font-size: 4em; color: white; padding-left: 0.3em; margin-top: 3px ;float:left"></span>

                             <span style="color: white; float: left; margin-top: 10px; margin-left: 10px;margin-right:80px;font-size: 13px;">Llamadas</span>
                         </a>
                          <a class="metrostyle orgmetro" style="cursor: pointer" href='{{URL::to("/citas")}}'>

                             <span class="far fa-calendar-alt" style="font-size: 4em; color: white; padding-left: 0.3em; margin-top: 3px ;float:left"></span>

                             <span style="color: white; float: left; margin-top: 10px; margin-left: 10px;margin-right:120px;font-size: 13px;">Citas</span>
                         </a>
                          <a class="metrostyle eenmetro" style="cursor: pointer" href='{{URL::to("/informes")}}'>

                             <span class="fas fa-chart-bar" style="font-size: 4em; color: white; padding-left: 0.3em; margin-top: 3px ;float:left"></span>

                             <span style="color: white; float: left; margin-top: 10px; margin-left: 10px;margin-right:80px;font-size: 13px;">Informes</span>
                         </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
