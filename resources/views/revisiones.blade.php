@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <h3 class="module-title"><i class="fa fa-table" aria-hidden="true"></i> Lista de llamadas para revisiones</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="border-top:2px solid #E0007D;">
                    <div class="form-group row">
                            <div class="col-md-12">
                                <label for="nombre"><i class="fas fa-search"></i> Buscar</label>
                                <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Escriba aquí para buscar en la tabla...">
                            </div>
                        </div>
                        <div id="info"></div>
                <div class="table-responsive">
                <table class="table" style = "font-size: 13px;">
                        <thead class="table-header">
                            <th>Matrícula</th>
                            <th>tipo</th>
                            <th>proxima_llamada</th>
                            <th>comentarios</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                            @foreach($revisiones as $revision)
                            <tr>
                      <td class="matricula">{{$revision->matricula}}</td>
                     <td><input type="hidden" class="tipo" value="{{$revision->tipo}}">{{$revision->nom_tipo}}</td>
                    <td><input type='date' class='form-control nueva_llamada' id='nueva_llamada' value="{{strftime('%Y-%m-%d',strtotime($revision->proxima_llamada))}}"></td>
                     <td><input type='text' class='form-control nuevo_comentario' id='nuevo_comentario' value='{{$revision->comentarios}}'></td>".
                     <td><input type='date' class='form-control nueva_cita' id='nueva_cita' value="{{strftime('%Y-%m-%d',strtotime($revision->fecha_cita))}}"></td>
                     <td><a class='btn btn-sm btn-success edit_revision' id='".$revision->tipo."' href='#'><i class='fas fa-edit'></i></a></td><tr>
                           </tr>
                            @endforeach
                        </tbody>
                    </table>
              </div>
              <div id="pagination" class="col-md-12 text-center">
              {{ $revisiones->links('vendor.pagination.pagination') }}      
              </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
      

$(function() {


$(document).on("keyup","#buscar",function() {

            $search = $(this).val();
  setTimeout(function () {
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('cliente/buscar')}}',
                    data: {'search': $search},
                    success: function (data) {
                        console.log(JSON.stringify(data));
                        $('tbody').html(data);
                        //$("#pagination").html(data[1]);
                    },
                    error: function (data) {
                        console.log(JSON.stringify(data));
                    }
                });
            }, 500);
});


$(document).on("click",".edit_revision",function(){


     var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Estamos guardando los cambios. Por favor, no cierre la página...</p>',
    closeButton: false

});

$matricula = $(this).closest("tr").find('.matricula').text();
$tipo = $(this).closest("tr").find('.tipo').val();
$nueva_llamada = $(this).closest("tr").find('.nueva_llamada').val();
$nuevo_comentario = $(this).closest("tr").find('.nuevo_comentario').val();
$nueva_cita = $(this).closest("tr").find('.nueva_cita').val();
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/vehiculo/guardar_revision')}}',
                data : {'matricula':$matricula,'tipo':$tipo,'nueva_llamada':$nueva_llamada,'nuevo_comentario':$nuevo_comentario,'nueva_cita':$nueva_cita},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $title = "Correcto";
                    if(data.includes("Error")){
                      bootbox.alert({
                        title: "Se ha producido un error",
                        message: data,
                         callback: function () {
                            return false;
                        }
                    });
                    }else{
                           bootbox.alert({
                        title: $title,
                        message: "Se ha modificado la revisión correctamente",
                         callback: function () {
                            location.reload();
                        }
                    });

                    }
                                  },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


});

    </script>
    @endsection