@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title"><i class="fas fa-car"></i> Registrar vehículo</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="submit" class='btn-link'><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver" class='btn-link'><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:1px solid #E0007D;">                        
                    <form class="inline-form" id="form-cliente">
                    	<div class="alert alert-danger alert-dismissable" style="display: none !important">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>¡Se ha producido un error!</strong> Debe rellenar todos los campos resaltados, por favor corríjalo y vuelva a intentarlo.
          </div>    
                    <div class="form-group row">
                        <div class="form-group col-md-4">
                        <label for="expediente">contacto </label>
                        <select class='form-control selecpicker' name="contacto">
                            <option selected disabled>Seleccionar...</option>
                        @foreach($contactos as $contacto)
                        <option value="{{$contacto->ID}}">{{$contacto->datos_personales}}</option>
                        @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="form-group row">
	                    <div class="form-group col-md-3">
	                            <label for="expediente">Matricula </label>
	                            <input type="text" class="form-control" name="matricula" value="" maxlength="8">
	                        </div>
	                        <div class="form-group col-md-3">
	                            <label for="expediente">Modelo </label>
	                            <input type="text" class="form-control" name="modelo" value="">
	                        </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">Fecha matriculación </label>
                                <input type="date" class="form-control" name="fecha_matriculacion" value="">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Fecha entrada taller </label>
                                <input type="date" class="form-control" name="fecha_entrada" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-md-3">
                                <label for="expediente">Asesor</label>
                                <select class="form-control selecpicker" name="asesor">
                                    <option selected disabled>Seleccionar...</option>
                                  @foreach($asesores as $asesor)
                        <option value="{{$asesor->ID}}">{{$asesor->nombre_asesor}}</option>
                        @endforeach   
                                </select>
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">Vía</label>
                                <select class="form-control selecpicker" name="via">
                                    <option selected disabled>Seleccionar...</option>
                                   @foreach($via_entrada as $via)
                        <option value="{{$via->ID}}">{{$via->nombre_via}}</option>
                        @endforeach  
                                </select>
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">Seguro</label>
                                <select class="form-control selecpicker" name="tipo_seguro">
                                     <option selected disabled>Seleccionar...</option>
                                    @foreach($tipos_seguro as $tipo)
                        <option value="{{$tipo->ID}}">{{$tipo->nombre_tipo}}</option>
                        @endforeach 
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="div_euros" style="display:none">
                                <label for="expediente">Euros</label>
                                <input type="number" class="form-control" name="euros" value="0" min="0">
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">KM recorridos</label>
                                <input type="number" class="form-control" name="kilometros" value="" min="0">
                            </div>
                        </div>
                              <span>(*) campos requeridos</span>
                    </form>
              
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function() {
  // Handler for .ready() called.
   $("#volver").click(function(){
        window.history.go(-1); return false;
    });

   $(document).on("change","select[name=tipo_seguro]",function(){

    if($("select[name=tipo_seguro] option:selected").val() == 2){
        $("#div_euros").css("display","block");
   }else{
        $("#div_euros").css("display","none");
   }

   });


$('input[name=matricula]').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});


    $("#submit").click(function(){
    $isValid = true;

    $("#form-cliente input").each(function(){

        if($(this).val() == ""){
              $(this).css("border-color","#f94646");
              $isValid = false;
        }else{
            $(this).css("border-color","#ccd0d2");
        }
      
    });

    $("#form-cliente select").each(function(){
        if($(this).val() == null){
              $(this).css("border-color","#f94646");
              $isValid = false;
        }else{
            $(this).css("border-color","#ccd0d2");
        }
      
    });

    if($isValid){
      var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Estamos enviando su solicitud. Por favor, no cierre la página...</p>',
    closeButton: false
});
// do something in the background
    $form = $("#form_checkin").serialize();
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/vehiculo/store')}}',
                data : {'datos' : $("#form-cliente").serialize()},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $title = "Correcto";
                    if(data.includes("Error")){
                      bootbox.alert({
                        title: "Se ha producido un error",
                        message: data,
                         callback: function () {
                            return false;
                        }
                    });
                    }else{
                           bootbox.alert({
                        title: $title,
                        message: "Se ha registrado el contacto correctamente",
                         callback: function () {
                            window.location.href = '/vehiculo/show/'+data;
                        }
                    });

                    }
                                  },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    }else{
            $(".alert-danger").css("display","");
    }

   

});

});

   
</script>
@endsection