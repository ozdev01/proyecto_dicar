@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title"><i class="fas fa-car"></i> {{$vehiculo->matricula}}</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="submit" class='btn-link'><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver" class='btn-link'><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:1px solid #E0007D;">                        
                    <form class="inline-form" id="form-cliente">
                    	<div class="alert alert-danger alert-dismissable" style="display: none !important">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>¡Se ha producido un error!</strong> Debe rellenar todos los campos resaltados, por favor corríjalo y vuelva a intentarlo.
          </div>    
                    <div class="form-group row">
                        <div class="form-group col-md-4">
                        <label for="expediente">contacto </label>
                        <select class='form-control selecpicker' name="contacto">
                        @foreach($contactos as $contacto)
                        @if($vehiculo->Id_cliente == $contacto->ID)
                        <option value="{{$contacto->ID}}" selected>{{$contacto->datos_personales}}</option>
                        @else
                        <option value="{{$contacto->ID}}">{{$contacto->datos_personales}}</option>
                        @endif
                        @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="form-group row">
	                    <div class="form-group col-md-3">
	                            <label for="expediente">Matricula </label>
	                            <input type="text" class="form-control" name="matricula" value="{{$vehiculo->matricula}}">
	                        </div>
	                        <div class="form-group col-md-3">
	                            <label for="expediente">Modelo </label>
	                            <input type="text" class="form-control" name="modelo" value="{{$vehiculo->modelo}}">
	                        </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">Fecha matriculación </label>
                                <input type="date" class="form-control" name="fecha_matriculacion" value="{{strftime('%Y-%m-%d',strtotime($vehiculo->fecha_matriculacion))}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Fecha entrada taller </label>
                                <input type="date" class="form-control" name="fecha_entrada" value="{{strftime('%Y-%m-%d',strtotime($vehiculo->fecha_entrada_taller))}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-md-3">
                                <label for="expediente">Asesor</label>
                                <select class="form-control selecpicker" name="asesor">
                                  @foreach($asesores as $asesor)
                            @if($vehiculo->asesor == $asesor->ID)
                        <option value="{{$asesor->ID}}" selected>{{$asesor->nombre_asesor}}</option>
                        @else
                        <option value="{{$asesor->ID}}">{{$asesor->nombre_asesor}}</option>
                        @endif
                        @endforeach   
                                </select>
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">Vía</label>
                                <select class="form-control selecpicker" name="via">
                                   @foreach($via_entrada as $via)
                                   @if($vehiculo->via == $via->ID)
                        <option value="{{$via->ID}}" selected>{{$via->nombre_via}}</option>
                        @else
                        <option value="{{$via->ID}}">{{$via->nombre_via}}</option>
                        @endif
                        @endforeach  
                                </select>
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">Seguro</label>
                                <select class="form-control selecpicker" name="tipo_seguro">
                                     <option selected disabled>Seleccionar...</option>
                                    @foreach($tipos_seguro as $tipo)
                                    @if($vehiculo->tipo_seguro == $tipo->ID)
                        <option value="{{$tipo->ID}}" selected>{{$tipo->nombre_tipo}}</option>
                        @else
                        <option value="{{$tipo->ID}}">{{$tipo->nombre_tipo}}</option>
                        @endif
                        @endforeach 
                                </select>
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">KM recorridos</label>
                                <input type="number" class="form-control" name="kilometros" value="{{$vehiculo->kilometros}}" min="0">
                            </div>
                        </div>
                              <span>(*) campos requeridos</span>
                    </form>
              
                    </div>                        
                </div>
            </div>
             <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="header">                    
                        <div class="col-md-8">
                            <h3 class="module-title"><i class="fas fa-wrench"></i> Seguimiento de revisiones</h3>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 10px">
                           <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="1" class='btn-link revisiones'><i class="fas fa-wrench"></i> Revisión</a></li>                                 
                                    <li><a href="#" id="2" class='btn-link revisiones'><i class="fas fa-dot-circle"></i> Neumáticos</a></li>
                                    <li><a href="#" id="3" class='btn-link revisiones'><i class="fas fa-box"></i> Kit</a></li>
                                    <li><a href="#" id="4" class='btn-link revisiones'><i class="fas fa-cog"></i> Amortiguadores</a></li>
                                    <li><a href="#" id="5" class='btn-link revisiones'><i class="fas fa-tint"></i> Pastillas</a></li>
                                    <li><a href="#" id="6" class='btn-link revisiones'><i class="fas fa-check-square"></i> ITV</a></li>
                                    <li><a href="#" id="7" class='btn-link revisiones'><i class="fas fa-cogs"></i> Varios</a></li>
                                    <li><a href="#" id="8" class='btn-link revisiones'><i class="fas fa-paint-brush"></i> Ch/Pt</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:1px solid #E0007D;">
                    <div class="table-responsive">
                <table class="table" style = "font-size: 13px;">
                        <thead class="table-header">
                            <th>Próxima llamada</th>
                            <th>comentarios</th>
                            <th>Próxima cita</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        
                        </tbody>
                    </table>
                   
              </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function() {
  // Handler for .ready() called.
   $("#volver").click(function(){
        window.history.go(-1); return false;
    });


   $(document).on("click",".revisiones",function(){
    $(".revisiones").each(function() {
  $( this ).css("border-bottom","transparent");
});

$(this).css("border-bottom","2px solid #DB012E");

$matricula = '{{$vehiculo->matricula}}';
$tipo = $(this).attr("id");

  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/vehiculo/revisiones')}}',
                data : {'matricula':$matricula,'tipo':$tipo},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#myTable").html(data);
                    },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


   });




    $("#submit").click(function(){
    $isValid = true;

    $("#form-cliente input").each(function(){

        if($(this).val() == ""){
              $(this).css("border-color","#f94646");
              $isValid = false;
        }else{
            $(this).css("border-color","#ccd0d2");
        }
      
    });

    $("#form-cliente select").each(function(){
        if($(this).val() == null){
              $(this).css("border-color","#f94646");
              $isValid = false;
        }else{
            $(this).css("border-color","#ccd0d2");
        }
      
    });

    if($isValid){
      var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Estamos enviando su solicitud. Por favor, no cierre la página...</p>',
    closeButton: false
});
// do something in the background
    $form = $("#form_checkin").serialize();
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/vehiculo/store')}}',
                data : {'datos' : $("#form-cliente").serialize()},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $title = "Correcto";
                    if(data.includes("Error")){
                      bootbox.alert({
                        title: "Se ha producido un error",
                        message: data,
                         callback: function () {
                            return false;
                        }
                    });
                    }else{
                           bootbox.alert({
                        title: $title,
                        message: "Se ha registrado el contacto correctamente",
                         callback: function () {
                            window.location.href = '/vehiculo/show/'+data;
                        }
                    });

                    }
                                  },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    }else{
            $(".alert-danger").css("display","");
    }

   

});

$(document).on("click",".edit_revision",function(){


     var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Estamos enviando su solicitud. Por favor, no cierre la página...</p>',
    closeButton: false

});

$matricula = '{{$vehiculo->matricula}}';
$tipo = $(this).attr("id");
$nueva_llamada = $("#nueva_llamada").val();
$nuevo_comentario = $("#nuevo_comentario").val();
$nueva_cita = $("#nueva_cita").val();
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/vehiculo/guardar_revision')}}',
                data : {'matricula':$matricula,'tipo':$tipo,'nueva_llamada':$nueva_llamada,'nuevo_comentario':$nuevo_comentario,'nueva_cita':$nueva_cita},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $title = "Correcto";
                    if(data.includes("Error")){
                      bootbox.alert({
                        title: "Se ha producido un error",
                        message: data,
                         callback: function () {
                            return false;
                        }
                    });
                    }else{
                           bootbox.alert({
                        title: $title,
                        message: "Se ha modificado la revisión correctamente",
                         callback: function () {
                            location.reload();
                        }
                    });

                    }
                                  },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


});
   
</script>
@endsection