@extends('layouts.calendar')
@section('content')
<!-- fullCalendar -->
  <link rel="stylesheet" href="fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="fullcalendar/dist/fullcalendar.print.min.css" media="print">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
           		<div id="calendar"></div>
                </div>
            </div>
        </div>
    </div> 
  <!-- jQuery 3 -->
<script src="jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="jquery-ui/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="fastclick/lib/fastclick.js"></script>

<!-- fullCalendar -->
<script src="moment/moment.js"></script>
<script src="fullcalendar/dist/fullcalendar.min.js"></script>
<script src="fullcalendar/dist/locale-all.js"></script>
<script src="fullcalendar/dist/locale/es.js"></script>


  <!-- Page specific script -->
<script>
  $(function () {

  	$(document).on("click",".fc-content",function(){

  		$matricula = $(this).find("span").text();

  		 setTimeout(function () {
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('cargar_cita')}}',
                    data: {'matricula': $matricula},
                    success: function (data) {
                        console.log(JSON.stringify(data));
                        bootbox.alert({
                        title : "Detalles de la cita",
					    message: data,
					    callback: function () {
					        
					    }
					})
                    },
                    error: function (data) {
                        console.log(JSON.stringify(data));
                    }
                });
            }, 500);


  	});

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({

      lang: 'es',

      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'Hoy',
        month: 'Mes',
        week : 'Semana',
        day  : 'Día'
      },events : {
      	url : 'http://localhost:8000/eventos'
      },
      //Random default events
      /*events    : [
        {
          title          : 'All Day Event',
          start          : new Date(y, m, 1),
          backgroundColor: '#f56954', //red
          borderColor    : '#f56954' //red
        },
        {
          title          : 'Long Event',
          start          : new Date(y, m, d - 5),
          end            : new Date(y, m, d - 2),
          backgroundColor: '#f39c12', //yellow
          borderColor    : '#f39c12' //yellow
        },
        {
          title          : 'Meeting',
          start          : new Date(y, m, d, 10, 30),
          allDay         : false,
          backgroundColor: '#0073b7', //Blue
          borderColor    : '#0073b7' //Blue
        },
        {
          title          : 'Lunch',
          start          : new Date(y, m, d, 12, 0),
          end            : new Date(y, m, d, 14, 0),
          allDay         : false,
          backgroundColor: '#00c0ef', //Info (aqua)
          borderColor    : '#00c0ef' //Info (aqua)
        },
        {
          title          : 'Birthday Party',
          start          : new Date(y, m, d + 1, 19, 0),
          end            : new Date(y, m, d + 1, 22, 30),
          allDay         : false,
          backgroundColor: '#00a65a', //Success (green)
          borderColor    : '#00a65a' //Success (green)
        },
        {
          title          : 'Click for Google',
          start          : new Date(y, m, 28),
          end            : new Date(y, m, 29),
          url            : 'http://google.com/',
          backgroundColor: '#3c8dbc', //Primary (light-blue)
          borderColor    : '#3c8dbc' //Primary (light-blue)
        }
      ],*/
      editable  : true,
      disableDragging: true,
      droppable : false, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove()
        }

      }
    })
  })
</script>
@endsection