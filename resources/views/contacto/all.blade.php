@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <h3 class="module-title"><i class="fa fa-table" aria-hidden="true"></i> Lista de contactos</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="border-top:2px solid #E0007D;">
                    <div class="form-group row">
                            <div class="col-md-12">
                                <label for="nombre"><i class="fas fa-search"></i> Buscar</label>
                                <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Escriba aquí para buscar en la tabla...">
                            </div>
                        </div>
                        <div id="info"></div>
                <div class="table-responsive">
                <table class="table" style = "font-size: 13px;">
                        <thead class="table-header">
                            <th>Nombre completo</th>
                            <th>Teléfono</th>
                            <th>Fecha de ultimo contacto</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                            @foreach($contactos as $contacto)
                            <tr>
                           <td>{{$contacto->datos_personales}}</td>
                           <td>{{$contacto->telefono}}</td>
                           <td>{{date('d-m-Y', strtotime($contacto->ultimo_contacto))}}</td>
                           <td><a class="btn btn-sm btn-success" href='{{URL::to("/contacto/show/$contacto->ID")}}'><i class="fas fa-external-link-alt"></i></a></td>
                           </tr>
                            @endforeach
                        </tbody>
                    </table>
              </div>
              <div id="pagination" class="col-md-12 text-center">
              {{ $contactos->links('vendor.pagination.pagination') }}      
              </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
      

$(function() {


$(document).on("keyup","#buscar",function() {

            $search = $(this).val();
  setTimeout(function () {
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('cliente/buscar')}}',
                    data: {'search': $search},
                    success: function (data) {
                        console.log(JSON.stringify(data));
                        $('tbody').html(data);
                        //$("#pagination").html(data[1]);
                    },
                    error: function (data) {
                        console.log(JSON.stringify(data));
                    }
                });
            }, 500);
});


});

    </script>
    @endsection