@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title"><i class="fas fa-user"></i> {{$contacto->datos_personales}}</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="submit" class='btn-link'><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver" class='btn-link'><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:1px solid #E0007D;">                        
                    <form class="inline-form" id="form-cliente">
                    	<div class="alert alert-danger alert-dismissable" style="display: none !important">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>¡Se ha producido un error!</strong> Debe rellenar todos los campos resaltados, por favor corríjalo y vuelva a intentarlo.
          </div>
                    <div class="form-group row">
                        <div class="form-group col-md-3">
                                <label for="expediente">Nombre</label>
                                <input type="text" class="form-control" name="nombre" value="{{$contacto->nombre}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Apellidos</label>
                                <input type="text" class="form-control" name="apellidos" value="{{$contacto->apellidos}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Fecha nacimiento</label>
                                <input type="date" class="form-control" name="fecha_nacimiento" value="{{strftime('%Y-%m-%d',strtotime($contacto->fecha_nacimiento))}}">
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">DNI</label>
                                <input type="text" class="form-control" name="dni" value="{{$contacto->DNI}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-md-4">
                                <label for="expediente">Dirección</label>
                                <input type="text" class="form-control" name="direccion" value="{{$contacto->direccion}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Cod. Postal</label>
                                <input type="number" class="form-control" name="cod_postal" value="{{$contacto->cod_postal}}" min='0' maxlength="5">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Provincia</label>
                                <select class='form-control' name='provincia'>
                                   @foreach($provincias as $provincia)
                                   @if($provincia->id_provincia == $contacto->provincia)
                                    <option value="provincia->id_provincia" selected>{{$provincia->nombre}}</option>
                                    @else
                                    <option value="provincia->id_provincia">{{$provincia->nombre}}</option>
                                    @endif
                                   @endforeach
                                </select>
                            </div>
                        </div>
                         <div class="form-group row">
                             <div class="form-group col-md-3">
                                <label for="expediente">Email</label>
                                <input type="text" class="form-control" name="email" value="{{$contacto->email}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">teléfono 1</label>
                                <input type="number" class="form-control" name="movil_1" value="{{$contacto->movil_1}}" min='6'>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Teléfono 2</label>
                                <input type="number" class="form-control" name="movil_2" value="{{$contacto->movil_1}}" min='6'>
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">último contacto</label>
                                <input type="date" class="form-control" name="ultimo_contacto" value="{{strftime('%Y-%m-%d',strtotime($contacto->ultimo_contacto))}}">
                            </div>
                         </div>
                              <span>(*) campos requeridos</span>
                    </form>
              
                    </div>                        
                </div>
            </div>
              <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="header">                    
                        <div class="col-md-8">
                            <h3 class="module-title"><i class="fas fa-truck"></i> Vehículos del cliente</h3>
                        </div>
                        <div class="col-md-4">
                           <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="vehiculos" class='btn-link'><i class="fas fa-car"></i> Vehiculos</a></li>                                 

                                    <li><a href="#" id="llamadas" class='btn-link'><i class="fas fa-phone"></i> Historial de llamadas</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:1px solid #E0007D;">
                    <div class="table-responsive">
                         <button class="btn-link"><i class="fas fa-plus"></i> Nuevo vehículo</button>
                <table class="table" style = "font-size: 13px;">
                        <thead class="table-header">
                            <th>Matrícula</th>
                            <th>Modelo</th>
                            <th>Fecha matr.</th>
                            <th>Asesor</th>
                            <th>Km</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach($vehiculos as $vehiculo)
                        <tr>
                        <td>{{$vehiculo->matricula}}</td>
                        <td>{{$vehiculo->modelo}}</td> 
                        <td>{{date('d-m-Y', strtotime($vehiculo->fecha_matriculacion))}}</td> 
                        <td>{{$vehiculo->asesor}}</td> 
                        <td>{{$vehiculo->kilometros}}</td> 
                        <td><a href="#" class="btn btn-success"><i class="fas fa-edit"></i></a></td>     
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                   
              </div> 
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">


   
	$(function() {
  // Handler for .ready() called.
   $("#volver").click(function(){
        window.history.go(-1); return false;
    });

   


   $('input[name=cod_postal]').keypress(function() {
    if (this.value.length >= 5) {
        return false;
    }
});

$(document).on("click","#vehiculos",function(){

$id_cliente = {{$contacto->ID}};
   var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Cargando...</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/contacto/vehiculos')}}',
                data : {'id_cliente' : $id_cliente},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $("#header").html(' <div class="col-md-8"><h3 class="module-title"><i class="fas fa-truck"></i> Vehículos del cliente</h3></div>');
                     $("#header").append('<div class="col-md-4"><div><ul class="nav navbar-nav"><li><a href="#" id="vehiculos" class="btn-link"><i class="fas fa-car"></i> Vehiculos</a></li><li><a href="#" id="llamadas" class="btn-link"><i class="fas fa-phone"></i> Historial de llamadas</a></li></ul></div></div>');
                    $(".table-responsive").html(data);
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


});

$(document).on("click","#llamadas",function(){

$id_cliente = {{$contacto->ID}};
   var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Cargando...</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/contacto/llamadas')}}',
                data : {'id_cliente' : $id_cliente},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $("#header").html(' <div class="col-md-8"><h3 class="module-title"><i class="fas fa-phone"></i> historial de llamadas</h3></div>');
                    $("#header").append('<div class="col-md-4"><div><ul class="nav navbar-nav"><li><a href="#" id="vehiculos" class="btn-link"><i class="fas fa-car"></i> Vehiculos</a></li><li><a href="#" id="llamadas" class="btn-link"><i class="fas fa-phone"></i> Historial de llamadas</a></li></ul></div></div>');
                    $(".table-responsive").html(data);
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


});
    



    $("#submit").click(function(){
    $isValid = true;

    $("#form-cliente input").each(function(){

        if($(this).val() == ""){
              $(this).css("border-color","#f94646");
              $isValid = false;
        }else{
            $(this).css("border-color","#ccd0d2");
        }
      
    });

    if($isValid){
      var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Estamos enviando su solicitud. Por favor, no cierre la página...</p>',
    closeButton: false
});
// do something in the background
    $form = $("#form_checkin").serialize();
    $id = {{$contacto->ID}};
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contacto/update/'+$id,
                data : {'datos' : $("#form-cliente").serialize()},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $title = "Correcto";
                    if(data.includes("Error")){
                      bootbox.alert({
                        title: "Se ha producido un error",
                        message: data,
                         callback: function () {
                            return false;
                        }
                    });
                    }else{
                           bootbox.alert({
                        title: $title,
                        message: data,
                         callback: function () {
                            location.reload();
                        }
                    });

                    }
                                  },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    }else{
            $(".alert-danger").css("display","");
    }

});


});
   
</script>
@endsection