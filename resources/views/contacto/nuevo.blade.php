@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title"><i class="fas fa-user-plus"></i> Registrar contacto</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="submit" class='btn-link'><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver" class='btn-link'><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:1px solid #E0007D;">                        
                    <form class="inline-form" id="form-cliente">
                    	<div class="alert alert-danger alert-dismissable" style="display: none !important">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>¡Se ha producido un error!</strong> Debe rellenar todos los campos resaltados, por favor corríjalo y vuelva a intentarlo.
          </div>
                    <div class="form-group row">
	                    <div class="form-group col-md-3">
	                            <label for="expediente">Nombre</label>
	                            <input type="text" class="form-control" name="nombre" value="">
	                        </div>
	                        <div class="form-group col-md-3">
	                            <label for="expediente">Apellidos</label>
	                            <input type="text" class="form-control" name="apellidos" value="">
	                        </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Fecha nacimiento</label>
                                <input type="date" class="form-control" name="fecha_nacimiento" value="">
                            </div>
                             <div class="form-group col-md-3">
                                <label for="expediente">DNI</label>
                                <input type="text" class="form-control" name="dni" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-md-3">
                                <label for="expediente">Dirección</label>
                                <input type="text" class="form-control" name="direccion" value="">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Cod. Postal</label>
                                <input type="number" class="form-control" name="cod_postal" value="" min='0' maxlength="5">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Provincia</label>
                                <select class='form-control' name='provincia'>
                                    <option selected disabled>Seleccionar...</option>
                                     <option value='alava'>Álava</option>
                                     <option value='albacete'>Albacete</option>
                                     <option value='alicante'>Alicante/Alacant</option>
                                     <option value='almeria'>Almería</option>
                                     <option value='asturias'>Asturias</option>
                                     <option value='avila'>Ávila</option>
                                     <option value='badajoz'>Badajoz</option>
                                     <option value='barcelona'>Barcelona</option>
                                     <option value='burgos'>Burgos</option>
                                     <option value='caceres'>Cáceres</option>
                                     <option value='cadiz'>Cádiz</option>
                                     <option value='cantabria'>Cantabria</option>
                                     <option value='castellon'>Castellón/Castelló</option>
                                     <option value='ceuta'>Ceuta</option>
                                     <option value='ciudadreal'>Ciudad Real</option>
                                     <option value='cordoba'>Córdoba</option>
                                     <option value='cuenca'>Cuenca</option>
                                     <option value='girona'>Girona</option>
                                     <option value='laspalmas'>Las Palmas</option>
                                     <option value='granada'>Granada</option>
                                     <option value='guadalajara'>Guadalajara</option>
                                     <option value='guipuzcoa'>Guipúzcoa</option>
                                     <option value='huelva'>Huelva</option>
                                     <option value='huesca'>Huesca</option>
                                     <option value='illesbalears'>Illes Balears</option>
                                     <option value='jaen'>Jaén</option>
                                     <option value='acoruña'>A Coruña</option>
                                     <option value='larioja'>La Rioja</option>
                                     <option value='leon'>León</option>
                                     <option value='lleida'>Lleida</option>
                                     <option value='lugo'>Lugo</option>
                                     <option value='madrid'>Madrid</option>
                                     <option value='malaga'>Málaga</option>
                                     <option value='melilla'>Melilla</option>
                                     <option value='murcia'>Murcia</option>
                                     <option value='navarra'>Navarra</option>
                                     <option value='ourense'>Ourense</option>
                                     <option value='palencia'>Palencia</option>
                                     <option value='pontevedra'>Pontevedra</option>
                                     <option value='salamanca'>Salamanca</option>
                                     <option value='segovia'>Segovia</option>
                                     <option value='sevilla'>Sevilla</option>
                                     <option value='soria'>Soria</option>
                                     <option value='tarragona'>Tarragona</option>
                                     <option value='santacruztenerife'>Santa Cruz de Tenerife</option>
                                     <option value='teruel'>Teruel</option>
                                     <option value='toledo'>Toledo</option>
                                     <option value='valencia'>Valencia/Valéncia</option>
                                     <option value='valladolid'>Valladolid</option>
                                     <option value='vizcaya'>Vizcaya</option>
                                     <option value='zamora'>Zamora</option>
                                     <option value='zaragoza'>Zaragoza</option>
                                </select>
                            </div>
                        </div>
                         <div class="form-group row">
                             <div class="form-group col-md-3">
                                <label for="expediente">Email</label>
                                <input type="text" class="form-control" name="email" value="">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">teléfono 1</label>
                                <input type="number" class="form-control" name="movil_1" value="" min='6'>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="expediente">Teléfono 2</label>
                                <input type="number" class="form-control" name="movil_2" value="" min='6'>
                            </div>
                         </div>
                              <span>(*) campos requeridos</span>
                    </form>
              
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function() {
  // Handler for .ready() called.
   $("#volver").click(function(){
        window.history.go(-1); return false;
    });





    $("#submit").click(function(){
    $isValid = true;

    /*$("#form-cliente input").each(function(){

        if($(this).val() == ""){
              $(this).css("border-color","#f94646");
              $isValid = false;
        }else{
            $(this).css("border-color","#ccd0d2");
        }
      
    });*/

    if($isValid){
      var dialog = bootbox.dialog({
    message: '<p class="text-center"><i class="fas fa-spinner fa-spin"></i> Estamos enviando su solicitud. Por favor, no cierre la página...</p>',
    closeButton: false
});
// do something in the background
    $form = $("#form_checkin").serialize();
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/contacto/store')}}',
                data : {'datos' : $("#form-cliente").serialize()},            
                success : function(data){
                    dialog.modal('hide');
                    console.log(JSON.stringify(data));
                    $title = "Correcto";
                    if(data.includes("Error")){
                      bootbox.alert({
                        title: "Se ha producido un error",
                        message: data,
                         callback: function () {
                            return false;
                        }
                    });
                    }else{
                           bootbox.alert({
                        title: $title,
                        message: "Se ha registrado el contacto correctamente",
                         callback: function () {
                            window.location.href = '/contacto/show/'+data;
                        }
                    });

                    }
                                  },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    }else{
            $(".alert-danger").css("display","");
    }

   

});


});
   
</script>
@endsection