@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Menú principal</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="list-group">
  <a href="/informes/contactos" class="list-group-item list-group-item-action"><i class="fas fa-file-excel"></i> Contactos</a>
  <a href="#" class="list-group-item list-group-item-action"><i class="fas fa-file-excel"></i> Vehículos</a>
  <a href="#" class="list-group-item list-group-item-action"><i class="fas fa-file-excel"></i> Revisiones</a>
  <a href="#" class="list-group-item list-group-item-action"><i class="fas fa-file-excel"></i> Citas</a>
</div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
