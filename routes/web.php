<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', function () {
    return view('home');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contacto/allitems', 'ContactosController@index');
Route::get('/contacto/nuevo', 'ContactosController@create');
Route::get('/contacto/store', 'ContactosController@store');
Route::get('/contacto/vehiculos', 'ContactosController@vehiculos');
Route::get('/contacto/llamadas', 'ContactosController@llamadas');
Route::get('/contacto/show/{id}', 'ContactosController@show');
Route::get('/contacto/update/{id}', 'ContactosController@update');


Route::get('/vehiculo/allitems', 'VehiculosController@index');
Route::get('/vehiculo/nuevo', 'VehiculosController@create');
Route::get('/vehiculo/store', 'VehiculosController@store');
Route::get('/vehiculo/revisiones', 'VehiculosController@revisiones');
Route::get('/vehiculo/guardar_revision', 'VehiculosController@guardar_revision');
Route::get('/vehiculo/show/{id}', 'VehiculosController@show');
Route::get('/vehiculo/update/{id}', 'VehiculosController@update');

Route::get('/historial', 'HistorialController@index');
Route::get('/revisiones', 'RevisionesController@index');
Route::get('/citas', 'CitasController@index');
Route::get('/eventos', 'CitasController@eventos');
Route::get('/cargar_cita', 'CitasController@cargar_cita');

Route::get('/informes', 'InformesController@index');

Route::get('/informes/contactos', function () {
    return view('informes/contactos');
});
});


